import express from "express";
export const router = express.Router();
export default { router };
//Generar ruta//
router.get("/", (req, res) => {
	const params = {
		numdocente: req.query.numdocente,
		nombre: req.query.nombre,
		domicilio: req.query.domicilio,
		nivel: req.query.nivel,
        pago: req.query.pago,
        hi: req.query.hi,
        hijos : req.query.hijos
	};
	res.render("index", params);
});

router.post("/", (req, res) => {
	const params = {
		numdocente: req.body.numdocente,
		nombre: req.body.nombre,
		domicilio: req.body.domicilio,
		nivel: req.body.nivel,
        pago: req.body.pago,
        hi: req.body.hi,
        hijos : req.body.hijos

	};
	res.render("index", params);
});
